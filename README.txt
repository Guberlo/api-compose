#1. If you have problem with docker image download process, please run this command
az acr login --name exaudi

#2. Edit /etc/hosts
127.0.0.1 exaudi-mysql exaudi-redis exaudi-zookeeper exaudi-azstorage

#3. Choose the exaudi-docker-be profile you prefer
#   Who are you? 
#   If you don't know who are you, please stop to work and retry tomorrow.
#   Otherwise select one of the available choices and runs build script: 
#	1 api dev, 
#	2 ui dev|api tester|be tester
./build-image.sh 1

#4. Check containers are up
docker ps

#5. Enjoy with exaudi-docker-be container environment
